using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using Quirco.PoLocalizer.Abstractions;

namespace Quirco.PoLocalizer
{
    /// <summary>
    /// provides a localization files from the content root folder.
    /// </summary>
    public class ContentRootPoFileLocationProvider : ILocalizationFileLocationProvider
    {
        private readonly IFileProvider _fileProvider;
        private readonly string _resourcesContainer;

        /// <summary>
        /// Creates a new instance of <see cref="ContentRootPoFileLocationProvider"/>.
        /// </summary>
        /// <param name="hostingEnvironment">The <see cref="IHostEnvironment"/>.</param>
        /// <param name="localizationOptions">The <see cref="LocalizationOptions"/>.</param>
        public ContentRootPoFileLocationProvider(IHostEnvironment hostingEnvironment, IOptions<PoLocalizerOptions> localizationOptions)
        {
            _fileProvider = !string.IsNullOrEmpty(localizationOptions.Value.ResourcesDirectory)
                ? new PhysicalFileProvider(localizationOptions.Value.ResourcesDirectory)
                : hostingEnvironment.ContentRootFileProvider;
            _resourcesContainer = localizationOptions.Value.ResourcesPath;
        }

        /// <inheritdocs />
        public IEnumerable<IFileInfo> GetLocations(string cultureName)
        {
            var directoryContents = _fileProvider.GetDirectoryContents(Path.Combine(_resourcesContainer, cultureName));
             foreach (var file in directoryContents)
             {
                 if (file.Name.EndsWith(".po"))
                     yield return file;
             }
        }
    }
}
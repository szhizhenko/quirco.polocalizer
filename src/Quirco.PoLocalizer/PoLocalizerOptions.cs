﻿using Microsoft.Extensions.Localization;

namespace Quirco.PoLocalizer;

public class PoLocalizerOptions : LocalizationOptions
{
    /// <summary>
    /// By default uses IHostEnvironment.ContentRootFileProvider
    /// </summary>
    public string ResourcesDirectory { get; set; }
}
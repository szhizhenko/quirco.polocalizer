# Quirco.PoLocalizer

Localization engine based on PO (Plain object) files.
Set of tools and framework to use PO files as localization source.

Why PO? There are lot of projects and tools supporting translation via PO files: 
- online portal for collaborative translation https://poeditor.com/ 
- free editor https://poedit.net/
- Django Rosetta https://django-rosetta.readthedocs.io/ - translation UI for Django projects

We have plans to make our own PO editor with Blazor.

## Installation

In your startup code add:
```csharp
builder.Services.AddPortableObjectLocalization(cfg=>cfg.ResourcesPath = "Locale");
```
or for custom directory instead IHostEnvironment.ContentRootFileProvider:
```csharp
builder.Services.AddPortableObjectLocalization(cfg =>
{
    cfg.ResourcesPath = "Locale";
    cfg.ResourcesDirectory = AppContext.BaseDirectory;
});
```

Then for your app after the routing configuration (`app.UseRouting();`) add:
```csharp
app.UseRequestLocalization(new RequestLocalizationOptions()
    .SetDefaultCulture("ru")
    .AddSupportedCultures("ru", "en")
    .AddSupportedUICultures("ru", "en"));
```

In your project create `Locale` folder with two subfolders (one for each culture), and put localization files (`*.po`) inside like this:
```
Locale
|-- ru
    |-- SomeModule.Core.po
|-- en
    |-- SomeModule.Core.po
```

Don't forget to set 'Copy to output folder' attribute.

## Usage
To use localization inject into your code `IStringLocalizaer<T> localizer` where `T` is calling type and use like this:
```csharp
localizer["text to translate"]
```
```po
msgid "Test string"
msgstr "Тестовая строка (ru)"
```

### Pluralization
also pluralization forms are available:
```csharp
localizer.Plural(1, "One book", "{0} books") // 1 книга
localizer.Plural(2, "One book", "{0} books") // 2 книги
localizer.Plural(5, "One book", "{0} books") // 5 книг
```
```po
msgid "One book"
msgid_plural "{0} books"
msgstr[0] "{0} книга"
msgstr[1] "{0} книги"
msgstr[2] "{0} книг"
```

### Local translation overriding
Generic parameter is required for case when you need to override local module localizations via `msgctxt` PO attribute. 
For example if we need separate translation of some term in module `MyProgram.RootModule` in class `SomeManager`, 
we can inject `IStringLocalizer<SomeManager>` into `SomeManager` class, and declare following translation in PO file:
```po
msgctxt "MyProgram.RootModule.SomeManager"
msgid "Some term"
msgstr "Term local translation"
```
